import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

const initialValues = {
  name: '',
  email: '',
  channel: '',
}

const validate = values => {
  let errors = {};
      if(!values.name) {
        errors.name = 'required!'
      }
      if(!values.email) {
        errors.email = 'required!'
      }
      if(!values.channel) {
        errors.channel = 'required!'
      }
      return errors;
}

const validationSchema = Yup.object({
  name: Yup.string().required('Required'),
  email: Yup.string().email('Invalid email').required('Required'),
  channel: Yup.string().required("Required")
})

const onSubmit = values => {
    console.log(values)
}

const OldYoutubeForm = () => {

  return (
    <Formik 
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
    >
      <Form>
        <label>Name</label>
        <Field
          type='text'
          id='name'
          name='name'
        />
        <ErrorMessage name='name' />
        <label>Email</label>
        <Field
          type='email'
          id='email'
          name='email'
        />
        <ErrorMessage name='email' />
        <label>Channel</label>
        <Field
          type='text'
          id='channel'
          name='channel'
        />
        <ErrorMessage name='channel' />
        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
};

export default OldYoutubeForm;
