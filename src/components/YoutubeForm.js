import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const initialValues = {
  name: '',
  email: '',
  channel: '',
}

const validate = values => {
  let errors = {};
      if(!values.name) {
        errors.name = 'required!'
      }
      if(!values.email) {
        errors.email = 'required!'
      }
      if(!values.channel) {
        errors.channel = 'required!'
      }
      return errors;
}

const validationSchema = Yup.object({
  name: Yup.string().required('Required'),
  email: Yup.string().email('Invalid email').required('Required'),
  channel: Yup.string().required("Required")
})

const YoutubeForm = () => {
  const formik = useFormik({
    initialValues,
    onSubmit: values => {
      console.log(values)
    },
    // validate,
    validationSchema
  });

  // console.log(formik.values)

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <label>Name</label>
        <input
          type='text'
          id='name'
          name='name'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.name}
        />
        {formik.errors.name && formik.touched.name && <div>{formik.errors.name}</div>}
        <label>Email</label>
        <input
          type='email'
          id='email'
          name='email'
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.email}
        />
        {formik.errors.email && formik.touched.email && <div>{formik.errors.email}</div>}
        <label>Channel</label>
        <input
          type='text'
          id='channel'
          name='channel'
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.channel}
        />
        {formik.errors.channel && formik.touched.channel && <div>{formik.errors.channel}</div>}
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default YoutubeForm;
